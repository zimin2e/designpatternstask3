using System;
using Xunit;
using DataLayer;
using System.Collections.Generic;
using Xunit.Abstractions;
using System.IO;

using DataLayer;

namespace Tests
{

    public class Tests__Command__FuelBurn: TestBase
    {
        public Tests__Command__FuelBurn(ITestOutputHelper testOutputHelper) : base(testOutputHelper){}

     
        [Fact]
        public void MandatoryActionTests()
        {
            DataRepository repository = new DataRepository();

            // Step 1. Initial set data in repository
            ModelForRepositoryUpdate_FuelBurn modelForRepositoryUpdate_initial = new ModelForRepositoryUpdate_FuelBurn();

            modelForRepositoryUpdate_initial.Fuel = 100;

            var repositorySaveAdapter = new RepositorySaveAdapter_FuelBurn(repository, modelForRepositoryUpdate_initial );
            
            repositorySaveAdapter.Save();

            // Step 3. Make changes by command 
            ModelForCommandUpdate_FuelBurn toUpdateCommandData = new ModelForCommandUpdate_FuelBurn();

             toUpdateCommandData.FuelToBurn =  10; //c

            Command_FuelBurn command = new Command_FuelBurn(repository, toUpdateCommandData);  

            command.Execute();    

            // Step 4. Get data from repository
            var Fuel = ( int ) repository.getValue( "Fuel" ).Value;

            // Test
            Assert.True( Fuel == 90 );

        }
    }
}
