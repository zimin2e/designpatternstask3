using System;
using Xunit;
using DataLayer;
using System.Collections.Generic;
using Xunit.Abstractions;
using System.IO;

using DataLayer;

namespace Tests
{
    public class Tests__Command__FuelCheck: TestBase
    {
        public Tests__Command__FuelCheck(ITestOutputHelper testOutputHelper) : base(testOutputHelper){}
     
        [Fact]
        public void MandatoryActionTests()
        {
            DataRepository repository = new DataRepository();


            // Step 1. Initial set data in repository
            repository.setValue( "Fuel", 15 );

            repository.setValue( "HaveFuel", true );     

            repository.setValue( "Fuel", 3 ); // Assume updated

            // Step 2. Read repository data back

            ModelForCommandUpdate_FuelCheck updateCommandData = new ModelForCommandUpdate_FuelCheck();

            Command_FuelCheck command = new Command_FuelCheck(repository, updateCommandData);  

            command.Execute();     
                 
            // Step 4. Get data from repository
            var HaveFuel = ( bool ) repository.getValue( "HaveFuel" ).Value;

            // Test
            Assert.True( HaveFuel == false );

        }
    }
}
