using System;
using Xunit;
using DataLayer;
using System.Collections.Generic;
using Xunit.Abstractions;
using System.IO;

using DataLayer;

namespace Tests
{

    public class Tests__Command__Movable: TestBase
    {
        public Tests__Command__Movable(ITestOutputHelper testOutputHelper) : base(testOutputHelper){}

     
        [Fact]
        public void MandatoryActionTests()
        {
            DataRepository repository = new DataRepository();

            // Step 1. Initial set data in repository
            ModelForRepositoryUpdate_Movable modelForRepositoryUpdate_initial = new ModelForRepositoryUpdate_Movable();

            modelForRepositoryUpdate_initial.Location[0] = 12;

            modelForRepositoryUpdate_initial.Location[1] = 5;

            modelForRepositoryUpdate_initial.Velocity = new int[] { -7, 3 };

           var repositorySaveAdapter = new RepositorySaveAdapter_Movable(repository, modelForRepositoryUpdate_initial );
           
            repositorySaveAdapter.Save();



            // Step 3. Make changes by command 
            ModelForCommandUpdate_Movable updateCommandData = new ModelForCommandUpdate_Movable();

             updateCommandData.time =  1; //c

            Command_Movable command = new Command_Movable(repository, updateCommandData);  

            command.Execute();    

            // Step 4. Get data from repository
            var Location    = ( int[] ) repository.getValue( "Location" ).Value;

            // Test
            Assert.True( Location[0] == 5 );
            Assert.True( Location[1] == 8 );
        }


    }
}
