using System;
using Xunit;
using DataLayer;
using System.Collections.Generic;
using Xunit.Abstractions;
using System.IO;

using DataLayer;

namespace Tests
{

    public class Tests__MacroCommand: TestBase
    {
        public Tests__MacroCommand(ITestOutputHelper testOutputHelper) : base(testOutputHelper){}

     
        [Fact]
        public void MandatoryActionTests()
        {            
            MacroCommand macroCommand = new MacroCommand();

            DataRepository repository = new DataRepository();

            repository.setValue( "HaveFuel", true );  



            /*************************************/
            /* Move command */
            /*************************************/
            ModelForRepositoryUpdate_Movable modelForRepositoryUpdate_initial = new ModelForRepositoryUpdate_Movable();

            modelForRepositoryUpdate_initial.Location[0] = 12;

            modelForRepositoryUpdate_initial.Location[1] = 5;

            modelForRepositoryUpdate_initial.Velocity = new int[] { -7, 3 };          
            
            var repositorySaveAdapter = new RepositorySaveAdapter_Movable(repository, modelForRepositoryUpdate_initial );
            
            repositorySaveAdapter.Save();


            ModelForCommandUpdate_Movable toUpCommandData = new ModelForCommandUpdate_Movable();

             toUpCommandData.time =  1; //c

            Command_Movable command_Moveable = new Command_Movable(repository, toUpCommandData);  

            macroCommand.commands.Add( command_Moveable );



            /*************************************/
            /* Fuel burn command */
            /*************************************/
            ModelForRepositoryUpdate_FuelBurn modelForRepositoryUpdate_initial_FuelBurn = new ModelForRepositoryUpdate_FuelBurn();

            modelForRepositoryUpdate_initial_FuelBurn.Fuel = 100;          
            
            new RepositorySaveAdapter_FuelBurn(repository, modelForRepositoryUpdate_initial_FuelBurn ).Save();

            ModelForCommandUpdate_FuelBurn toUpdateCommandDataFuelBurn = new ModelForCommandUpdate_FuelBurn();

            toUpdateCommandDataFuelBurn.FuelToBurn =  97; //c           

            Command_FuelBurn commandFuelBurn = new Command_FuelBurn(repository, toUpdateCommandDataFuelBurn);  

            macroCommand.commands.Add( commandFuelBurn);



            /*************************************/
            /* Fuel check command */
            /*************************************/
            ModelForRepositoryUpdate_FuelCheck repositoryModel =  new RepositoryReadAdapter_FuelCheck(repository).Read();;
            
            repositoryModel.HaveFuel= true;

            new RepositorySaveAdapter_FuelCheck(repository, repositoryModel).Save();

            ModelForCommandUpdate_FuelCheck toUpdateCommandData = new ModelForCommandUpdate_FuelCheck();

            Command_FuelCheck command_FuelCheck = new Command_FuelCheck(repository, toUpdateCommandData);  

            macroCommand.commands.Add( command_FuelCheck );
     
            macroCommand.Execute(); 



            /*************************************/
            /* Tests */
            /*************************************/
            var Location    = ( int[] ) repository.getValue( "Location" ).Value;

            Assert.True( Location[0] == 5 );

            Assert.True( Location[1] == 8 );

            var Fuel = ( int ) repository.getValue( "Fuel" ).Value;

            Assert.True( Fuel == 3 );

            var HaveFuel = ( bool ) repository.getValue( "HaveFuel" ).Value;

            Assert.True( HaveFuel == false );
        }


    }
}
