using System;
using Xunit;
using DataLayer;
using System.Collections.Generic;
using Xunit.Abstractions;
using System.IO;

using DataLayer;

namespace Tests
{
  


    public class TestBase
    {

        private readonly ITestOutputHelper _testOutputHelper;

        public TestBase(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;

            Console.SetOut(new ConsoleWriter(_testOutputHelper));
        }

     

    }
}
