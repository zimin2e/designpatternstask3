﻿using System;

namespace DataLayer
{
    public class Command_Movable : ICommand
    {
        DataRepository repository;
        ModelForCommandUpdate_Movable commandModel;

        public Command_Movable(
            DataRepository repository, 
            ModelForCommandUpdate_Movable commandModel)
        {
            this.repository    = repository;    
            this.commandModel  = commandModel;    
        }

        public void Execute() 
        {
            var repositoryReadAdapter = new RepositoryReadAdapter_Movable(repository);

            ModelForRepositoryUpdate_Movable repositoryModel =  repositoryReadAdapter.Read();

            if (repositoryModel == null || commandModel == null ) throw new ArgumentException();

            if ( repositoryModel.Vector.Length != 2) throw new ArgumentException();
           
            if (repositoryModel.Location == null || repositoryModel.Location.Length != 2) throw new ArgumentException();

                      if (repositoryModel == null || commandModel == null ) throw new ArgumentException();

            if (repositoryModel.Velocity == null || repositoryModel.Velocity.Length != 2) throw new ArgumentException();
            if (repositoryModel.Location == null || repositoryModel.Location.Length != 2) throw new ArgumentException();

            if (repositoryModel.Velocity[0] > 100) throw new ArgumentException();
            if (repositoryModel.Velocity[1] > 100) throw new ArgumentException();

            if (repositoryModel.Location[0] < 0) throw new ArgumentException();
            if (repositoryModel.Location[1] < 0) throw new ArgumentException();

            repositoryModel.Location[0] += repositoryModel.Velocity[0] * commandModel.time;
            repositoryModel.Location[1] += repositoryModel.Velocity[1] * commandModel.time;

            new RepositorySaveAdapter_Movable(repository, repositoryModel ).Save();
        
        }

    }

}
