﻿using System;
using System.Collections.Generic;

namespace DataLayer
{
    public class MacroCommand : ICommand
    {
        public List<ICommand> commands = new List<ICommand>(); 

        public void Execute() 
        {
            foreach(var command in commands)
            {
                command.Execute();
            }

        }
    }  
}
