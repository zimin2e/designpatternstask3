﻿using System;

namespace DataLayer
{
    public class Command_FuelCheck : ICommand
    {
        public int FuelLimit = 10;
        DataRepository repository;
        ModelForCommandUpdate_FuelCheck commandModel;

        public Command_FuelCheck(
            DataRepository repository,
            ModelForCommandUpdate_FuelCheck commandModel)
        {
            this.repository    = repository;    
            this.commandModel       = commandModel;    
        }

        public void Execute() 
        {
            ModelForRepositoryUpdate_FuelCheck repositoryModel =  new RepositoryReadAdapter_FuelCheck(repository).Read();

            if (repositoryModel == null || commandModel == null ) throw new ArgumentException();

            if (repositoryModel.Fuel < 0) throw new ArgumentException();

            if (repositoryModel.Fuel < commandModel.FuelLimit) repositoryModel.HaveFuel = false;

            new RepositorySaveAdapter_FuelCheck(repository, repositoryModel).Save();
        }
    }  
}