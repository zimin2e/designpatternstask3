﻿using System;

namespace DataLayer
{
    public class Command_FuelBurn : ICommand
    {
        DataRepository repository;
        ModelForCommandUpdate_FuelBurn commandModel;

        public Command_FuelBurn(
            DataRepository repository,
            ModelForCommandUpdate_FuelBurn commandModel)
        {
            this.repository    = repository;    
            this.commandModel  = commandModel;    
        }

        public void Execute() 
        {
            var repositoryReadAdapter = new RepositoryReadAdapter_FuelBurn(repository);
            
            ModelForRepositoryUpdate_FuelBurn repositoryModel =  repositoryReadAdapter.Read();

            if (repositoryModel == null || commandModel == null ) throw new ArgumentException();

            repositoryModel.Fuel -= commandModel.FuelToBurn;

            new RepositorySaveAdapter_FuelBurn(repository, repositoryModel ).Save();
        }

    }

}
