﻿using System;

namespace DataLayer
{
    public class ModelForRepositoryUpdate_FuelCheck 
    {
        public bool HaveFuel { get; set; }       
        public int Fuel { get; set;}          

    }

}
