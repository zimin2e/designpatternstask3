﻿using System;

namespace DataLayer
{
    public class ModelForRepositoryUpdate_Movable 
    {
        public int[] Vector { get; set; } = new int[2];
        public int[] Location { get; set; } = new int[2];
        public int[] Velocity { get; set; } = new int[2];

    }

}
