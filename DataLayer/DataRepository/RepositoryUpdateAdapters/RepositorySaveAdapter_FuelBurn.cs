﻿using System;

namespace DataLayer
{
    public class RepositorySaveAdapter_FuelBurn :  IRepositorySave
    {
        IReposotory repository;
        ModelForRepositoryUpdate_FuelBurn model;

        public RepositorySaveAdapter_FuelBurn(
            IReposotory repository,
            ModelForRepositoryUpdate_FuelBurn model
         ) 
        {
            this.repository = repository;
            this.model = model;
        }


        public void Save()
        {         
            repository.setValue( "Fuel", model.Fuel);                   
        }        

    }

}
