﻿using System;

namespace DataLayer
{
    public class RepositorySaveAdapter_FuelCheck :  IRepositorySave
    {
        IReposotory repository;
        ModelForRepositoryUpdate_FuelCheck model;

        public RepositorySaveAdapter_FuelCheck(
            IReposotory repository,
            ModelForRepositoryUpdate_FuelCheck model
         ) 
        {
            this.repository = repository;
            this.model = model;
        }


        public void Save()
        {         
            repository.setValue( "HaveFuel", model.HaveFuel);                    
        }        

    }

}
