using System;

namespace DataLayer
{

    public interface IRepositoryRead<T>
    {
        public T Read();        
    }
}