﻿using System;

namespace DataLayer
{
    public class RepositorySaveAdapter_Movable :  IRepositorySave
    {
        IReposotory repository;
        ModelForRepositoryUpdate_Movable model;

        public RepositorySaveAdapter_Movable(
            IReposotory repository,
            ModelForRepositoryUpdate_Movable model
         ) 
        {
            this.repository = repository;
            this.model = model;
        }


        public void Save()
        {         
            repository.setValue( "Vector",     model.Vector);
            repository.setValue( "Location",   model.Location);
            repository.setValue( "Velocity",   model.Velocity);                       
        }        

    }

}
