﻿using System;

namespace DataLayer
{
    public class RepositoryReadAdapter_FuelCheck : IRepositoryRead<ModelForRepositoryUpdate_FuelCheck> 
    {
        IReposotory repository;
        public RepositoryReadAdapter_FuelCheck(IReposotory repository) 
        {
            this.repository = repository;
        }

        public ModelForRepositoryUpdate_FuelCheck Read()
        {
            return new ModelForRepositoryUpdate_FuelCheck
            {                    
                HaveFuel    = ( bool ) repository.getValue( "HaveFuel" ).Value,
                Fuel        = ( int ) repository.getValue( "Fuel" ).Value                 
            };
        }  

    }
}
