﻿using System;

namespace DataLayer
{
    public class RepositoryReadAdapter_Movable : IRepositoryRead<ModelForRepositoryUpdate_Movable> 
    {
        IReposotory repository;
        public RepositoryReadAdapter_Movable(IReposotory repository) 
        {
            this.repository = repository;
        }

        public ModelForRepositoryUpdate_Movable Read()
        {
            return new ModelForRepositoryUpdate_Movable
            {                    
                Vector      = ( int[] ) repository.getValue( "Vector" ).Value,
                Location    = ( int[] ) repository.getValue( "Location" ).Value,
                Velocity    = ( int[] ) repository.getValue( "Velocity" ).Value          
            };
        }  

    }
}
