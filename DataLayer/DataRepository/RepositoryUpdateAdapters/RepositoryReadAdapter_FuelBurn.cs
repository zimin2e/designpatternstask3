﻿using System;

namespace DataLayer
{
    public class RepositoryReadAdapter_FuelBurn : IRepositoryRead<ModelForRepositoryUpdate_FuelBurn> 
    {
        IReposotory repository;
        public RepositoryReadAdapter_FuelBurn(IReposotory repository) 
        {
            this.repository = repository;
        }

        public ModelForRepositoryUpdate_FuelBurn Read()
        {
            return new ModelForRepositoryUpdate_FuelBurn
            {                    
                Fuel  = ( int ) repository.getValue( "Fuel" ).Value,      
            };
        }  

    }
}
